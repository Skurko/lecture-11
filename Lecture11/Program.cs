﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Lecture11
{
    class Program
    {
        static void Main(string[] args)
        {
            var doc = new XDocument();
            var root = new XElement("Card");
            root.Add(new XAttribute("SynCode", "666"));
            root.Add(new XAttribute("Name", "From XML 1 UPDATED FOR THIRD TIME"));
            root.Add(new XAttribute("BranchId", "4"));
            root.Add(new XAttribute("Status", "1"));

            var contactsElement = new XElement("Contacts");
            var phone = new XElement("Phone");
            phone.Add(new XAttribute("Value", "(666) 666-666-666"));
            var email = new XElement("Email");
            email.Add(new XAttribute("Value", "666@666.ru"));
            var email2 = new XElement("Email");
            email2.Add(new XAttribute("Value", "new@mail.ru"));
            var phone2 = new XElement("Phone");
            phone2.Add(new XAttribute("Value", "(999) 999-999-999"));


            contactsElement.Add(phone);
            contactsElement.Add(email);
            contactsElement.Add(email2);
            contactsElement.Add(phone2);

            root.Add(contactsElement);

            doc.Add(root);

            Console.WriteLine(doc.ToString());

            Console.ReadKey();

            XmlRepository.LoadXml(doc);

            Console.ReadKey();
        }
    }
}
