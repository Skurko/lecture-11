﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Lecture11
{
    public static class XmlRepository
    {
        public static void LoadXml(XDocument doc)
        {
            using (var dbContext = new TestDBEntities())
            {
                var xmlCardSyncode = Convert.ToInt64(doc.Element("Card").Attribute("SynCode").Value);

                if (!dbContext.Cards.Any(c => c.SynCode == xmlCardSyncode))
                {
                    var card = new Card();
                    UpdateCard(card, doc);

                    foreach (var element in doc.Element("Card").Element("Contacts").Elements())
                    {
                        var newContact = new Contact
                        {
                            Value = element.Attribute("Value").Value,
                            ContactTypeId = GetContactTypeId(element.Name)
                        };

                        card.Contacts.Add(newContact);
                    }

                    dbContext.Cards.Add(card);
                    dbContext.SaveChanges();
                }
                else
                {
                    var contactsToDelete = new List<Contact>();

                    var card = dbContext.Cards.First(c => c.SynCode == xmlCardSyncode);
                    UpdateCard(card, doc);

                    foreach (var contact in card.Contacts)
                    {
                        if (!doc.Element("Card").Element("Contacts").Elements().Any(e => XNode.DeepEquals(e, ContactToXElement(contact))))
                        {
                            contactsToDelete.Add(contact);
                        }
                    }

                    foreach (var contact in contactsToDelete)
                    {
                        card.Contacts.Remove(contact);
                        dbContext.Contacts.Remove(contact);
                    }

                    foreach (var element in doc.Element("Card").Element("Contacts").Elements())
                    {
                        if (!card.Contacts.Any(c => c.ContactTypeId == GetContactTypeId(element.Name) && c.Value == element.Attribute("Value").Value))
                        {
                            card.Contacts.Add(new Contact
                        {
                            Value = element.Attribute("Value").Value,
                            ContactTypeId = GetContactTypeId(element.Name)
                        });
                        }
                    }
                    
                    dbContext.SaveChanges();
                }
            }
        }

        private static void UpdateCard(Card card, XDocument doc)
        {
            card.Name = doc.Element("Card").Attribute("Name").Value;
            card.BranchId = Convert.ToInt64(doc.Element("Card").Attribute("BranchId").Value);
            card.SynCode = Convert.ToInt64(doc.Element("Card").Attribute("SynCode").Value);
            card.StatusTypeId = Convert.ToInt32(doc.Element("Card").Attribute("Status").Value);
        }

        private static int GetContactTypeId(XName name)
        {
            using (var dbContext = new TestDBEntities())
            {
                return dbContext.ContactType.First(c => c.Name == name.LocalName).Id;
            }
        }

        private static XElement ContactToXElement(Contact contact)
        {
            string contactTypeName;

            using (var dbContext = new TestDBEntities())
            {
                contactTypeName = dbContext.ContactType.First(ct => ct.Id == contact.ContactTypeId).Name;
            }

            var element = new XElement(contactTypeName);
            element.Add(new XAttribute("Value", contact.Value));

            return element;
        }
    }
}
